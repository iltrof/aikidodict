Небольшой словарик по айкидо. Доступен по ссылке: https://iltrof.gitlab.io/aikidodict/, или в файле [aikido.md](https://gitlab.com/iltrof/aikidodict/blob/master/aikido.md).

A small aikido glossary in Russian.
