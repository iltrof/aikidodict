const soundVariants = [
  ["a", "а"],
  ["e", "е", "э", "є"],
  ["i", "y", "и", "й", "ь", "і", "ї"],
  ["o", "о", "wo", "во"],
  ["u", "у", "ы"],
  [
    "ya",
    "ia",
    "iya",
    "я",
    "ья",
    "ьа",
    "йа",
    "йя",
    "ьйа",
    "ьйя",
    "ъя",
    "ъйа",
    "ъйя",
    "иа",
    "ия",
    "ийа",
    "ийя"
  ],
  [
    "yo",
    "io",
    "iyo",
    "ё",
    "ьё",
    "ьо",
    "йо",
    "йё",
    "ьйо",
    "ьйё",
    "ъё",
    "ъйо",
    "ъйё",
    "ио",
    "иё",
    "ийо",
    "ийё"
  ],
  [
    "yu",
    "iu",
    "iyu",
    "ю",
    "ью",
    "ьу",
    "йу",
    "йю",
    "ьйу",
    "ьйю",
    "ъю",
    "ъйу",
    "ъйю",
    "иу",
    "ию",
    "ийу",
    "ийю"
  ],
  ["k", "к"],
  ["s", "sh", "с", "ш", "щ"],
  ["t", "ts", "c", "ch", "т", "ц", "ч", "тс"],
  ["n", "н"],
  ["h", "f", "х", "ф"],
  ["m", "м"],
  ["r", "l", "р", "л"],
  ["w", "в"],
  ["g", "г", "ґ"],
  ["z", "dz", "dj", "j", "з", "дз", "дж", "ж"],
  ["d", "д"],
  ["b", "б"],
  ["p", "п"]
];

const vowels = ["a", "e", "i", "o", "u", "ya", "yo", "yu"];

const repls = {
  ō: "ou",
  ū: "uu",
  wo: "o",
  cha: "chya",
  chu: "chyu",
  cho: "chyo",
  sha: "shya",
  shu: "shyu",
  sho: "shyo",
  ja: "jya",
  ju: "jyu",
  jo: "jyo",
  dja: "jya",
  dju: "jyu",
  djo: "jyo",
  mp: "np",
  mb: "nb",
  mm: "nm",
  во: "o",
  ча: "чя",
  чу: "чю",
  чо: "чё",
  ша: "шя",
  ща: "щя",
  шу: "шю",
  щу: "щю",
  шо: "шё",
  що: "щё",
  жа: "жя",
  жу: "жю",
  жо: "жё",
  джа: "жя",
  джу: "жю",
  джо: "жё",
  мп: "нп",
  мб: "нб",
  мм: "нм",
  ъ: "",
  ш: "шь",
  щ: "щь",
  ч: "чь"
};

splitIntoSounds = function(word) {
  let sounds = [];
  while (word.length !== 0) {
    let matches = [];
    for (vrts of soundVariants) {
      for (snd of vrts) {
        if (word.startsWith(snd)) {
          matches.push([snd, vrts[0]]);
        }
      }
    }
    matches = matches.sort((x, y) => y[0].length - x[0].length);
    if (matches.length === 0) {
      return undefined;
    }
    sounds.push(matches[0][1]);
    word = word.substr(matches[0][0].length);
  }
  return sounds;
};

cleanSounds = function(sounds) {
  let cleaned = [];
  let lastSnd = undefined;
  for (snd of sounds) {
    if (lastSnd && snd === lastSnd[lastSnd.length - 1]) {
      continue; // skip repeated letters and elongated sounds
    }
    if (lastSnd && snd === "u" && lastSnd[lastSnd.length - 1] === "o") {
      continue; // skip elongated 'o' as well
    }

    if (
      !vowels.includes(snd) &&
      lastSnd &&
      !vowels.includes(lastSnd) &&
      lastSnd !== "n"
    ) {
      cleaned.push("u"); // insert 'u' between consonants, but not after 'n'
    }
    cleaned.push(snd);
    lastSnd = snd;
  }
  return cleaned
    .join("")
    .replace(/wu/g, "u")
    .replace(/wi/g, "ui")
    .replace(/we/g, "ue");
};

normalize = function(word) {
  word = word.toLowerCase();
  word = word.replace(/[^a-zа-яёōūєіїґ]/g, "");
  for (k in repls) {
    word = word.split(k).join(repls[k]);
  }

  const sounds = splitIntoSounds(word);
  if (sounds === undefined) {
    return undefined;
  }
  return cleanSounds(sounds);
};

sections = [
  ...document.getElementsByClassName("section2"),
  ...document.getElementsByClassName("section3")
];
toc = document.getElementsByClassName("toc")[0];
rows = [...document.querySelectorAll("tbody > tr")];
words = rows.map(x => [
  normalize(
    x.querySelector("td:nth-child(2)").innerText.replace(/[^a-zōū]/gi, "")
  ),
  x.querySelector("td:nth-child(3)").innerText.toLowerCase(),
  x
]);
paragraphs = document.getElementsByTagName("p");

filterContent = function(query) {
  query = query.toLowerCase();
  let normalized = normalize(query);
  for (word of words) {
    if (word[0].includes(normalized) || word[1].includes(query)) {
      word[2].classList.remove("hidden");
      word[2].classList.add("found");
    } else {
      word[2].classList.add("hidden");
      word[2].classList.remove("found");
    }
  }
  for (sect of sections) {
    if (sect.querySelector(".found") === null) {
      sect.classList.add("hidden");
    } else {
      sect.classList.remove("hidden");
    }
  }
  for (p of paragraphs) {
    p.classList.add("hidden");
  }
  toc.classList.add("hidden");
};

resetContent = function() {
  for (word of words) {
    word[2].classList.remove("hidden");
    word[2].classList.remove("found");
  }
  for (sect of sections) {
    sect.classList.remove("hidden");
  }
  for (p of paragraphs) {
    p.classList.remove("hidden");
  }
  toc.classList.remove("hidden");
};

search = document.getElementById("search");
if (search.value.trim().length !== 0) {
  filterContent(search.value);
}

search.addEventListener("input", function(e) {
  let query = e.target.value.trim();
  if (query.length === 0) {
    resetContent();
  } else {
    filterContent(query);
  }
});
