import codecs
import markdown

template = """
<html>
<head>
<meta charset='utf-8'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<title>Глоссарий Айкидо</title>
<link rel='stylesheet' href='style.css'>
<script src='search.js' defer></script>
</head>
<body class='markdown-body'>
{}
</body>
</html>
""".strip()

md = markdown.Markdown(extensions=['mdx_outline', 'tables', 'toc'])

with codecs.open('aikido.md', 'r', 'utf-8') as f:
    text = f.read()

with codecs.open('public/index.html', 'w', 'utf-8') as f:
    html = md.convert(text)
    html = html.replace('<p>[Search]</p>',
                        '<input id="search" placeholder="Поиск слов..." />')
    f.write(template.format(html))
